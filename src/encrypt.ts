import * as aladinjs from 'aladinjs';

export function encryptBackupPhrase(plaintextBuffer: string, password: string) : Promise<Buffer> {
  return aladinjs.encryptMnemonic(plaintextBuffer, password);
}

export function decryptBackupPhrase(dataBuffer: string | Buffer, password: string) : Promise<string> {
  return aladinjs.decryptMnemonic(dataBuffer, password);
}

